var mongoose = require('mongoose');
var Bicicleta = require('../../models/bicicleta');

describe('Testing Bicicletas', function () {
  beforeEach(function (done) {
    var mongoDB = 'mongodb://localhost/red_bicicletas';
    mongoose.connect(mongoDB, { useNewUrlParser: true });
    var db = mongoose.connection;
    db.on('error', console.error.bind(console, 'MongoDB conection error..'));
    db.once('open', function () {
      console.log("we are connected to mongo DB");
      done()
    });
  });

  afterEach(function (done) {
    Bicicleta.deleteMany({}, function (err, success) {
      if (err) console.log(err);
      done()
    });
  });

  describe('Bici create instance', () => {
    it('create one instance of bici', () => {
      var bici = Bicicleta.createInstance(1, "verde", "urban", "cali", [34.5, 33.25]);
      expect(bici.code).toBe(1);
    });
  });

  describe('Bicicleta.allBici', () => {
    it('comienza vacia', (done) => {
      Bicicleta.allBicis(function(err, bicis){
        expect(bicis.length).toBe(0);
        done();
      });
    });
  });

});







/*
var request = require('request');

var server = require('../../bin/www');
const { json } = require('express');

beforeEach(() => {
  Bicicleta.allBicis = [];
  console.log("Ejecutando test...");
})


describe("test API Bicicletas", () => {
  describe("Test GET", () => {
    it("code status and body", () => {
      request.get('http://localhost:3000/api/bicicletas', function (error, res, body) {
        expect(res.statusCode).toBe(200);
        expect(JSON.parse(body)['bicicletas'].length).toBe(0)
      });
    });
  });

  describe("Test POST", () => {
    it("create one bici", (done) => {
      expect(Bicicleta.allBicis.length).toEqual(0);
      var bici = '{"id":101,"color":"verde","modelo":"montain","ciudad":"medallin","lat": 34.3434,"lng": 36.2536}';
      var headers = {"Content-Type": "application/json"};
      var url = "http://localhost:3000/api/bicicletas/create";

      request.post({headers: headers, url: url, body: bici}, function(error, res, body){
          expect(Bicicleta.allBicis.length).toEqual(1);
          expect(Bicicleta.allBicis[0].id).toBe(101);
          expect(Bicicleta.allBicis[0].color).toBe("verde");
          expect(Bicicleta.allBicis[0].modelo).toBe("montain");
          expect(Bicicleta.allBicis[0].ciudad).toBe("medallin");
          expect(Bicicleta.allBicis[0].ubicacion).toEqual([34.3434, 36.2536]);
          done();
      });
    });
  });

  describe("Test DELETE", () => {
    it("delete one object", (done) => {
      expect(Bicicleta.allBicis.length).toEqual(0);
      var headers = {"Content-Type": "application/json"};
      var url = "http://localhost:3000/api/bicicletas/delete";
      var body_id = '{"id": 1}'

      var a = new Bicicleta(1, "amarilla", "urban", "cali", [11.016565, -74.790201]);
      Bicicleta.add(a);


      request.delete({headers: headers, url: url, body: body_id}, function(error, res, body){
        expect(Bicicleta.allBicis.length).toEqual(0);
        done();
      });
    });
  });
});

*/