var mongoose = require('mongoose');
var Bicicleta = require('../../models/bicicleta');

describe('Testing Bicicletas', function () {

  beforeAll((done) => { mongoose.connection.close(done) });

  beforeEach(function (done) {
    originalTimeout = jasmine.DEFAULT_TIMEOUT_INTERVAL;
    jasmine.DEFAULT_TIMEOUT_INTERVAL = 10000;
    var mongoDB = 'mongodb://localhost/testdb';
    mongoose.connect(mongoDB, { useNewUrlParser: true });

    const db = mongoose.connection;
    db.on('error', console.error.bind(console, 'conection error..'));
    db.once('open', function () {
      console.log("we are connected to mongo DB");
      done()
    });
  });

  afterEach(function (done) {
    jasmine.DEFAULT_TIMEOUT_INTERVAL = originalTimeout;
    Bicicleta.deleteMany({}, function (err, success) {
      if (err) console.log(err);
      mongoose.disconnect();
      done()
    });
  });

  describe('Bici create instance', () => {
    it('create one instance of bici', () => {
      var bici = Bicicleta.createInstance(1, "verde", "urban", [34.5, 33.25]);
      expect(bici.code).toBe(1);
    });
  });

  describe('Bicicleta.allBici', function(){
    it('comienza vacia', function(){
      Bicicleta.allBicis(function(err, bicis){
        expect(bicis.length).toBe(0);

      });
    });
  });

});
