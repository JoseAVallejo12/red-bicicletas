var mongoose = require('mongoose');
var Schema = mongoose.Schema;
/*  index: {type: '2dsphere', sparse: true} */
var bicicletaSchema = new Schema({
  code: Number,
  color: String,
  modelo: String,
  ubicacion: {
    type: [Number],
    createIndex: {
      type: '2dsphere',
      sparse: true
    }
  }
});

bicicletaSchema.statics.createInstance = function (code, color, modelo, ubicacion) {
  return new this({
    code: code,
    color: color,
    modelo: modelo,
    ubicacion: ubicacion
  });
};

bicicletaSchema.methods.toString = function () {
  return `id: ${this.id} | color: ${this.color}`;
}

bicicletaSchema.statics.allBicis = async function (cb) {
  return await mongoose.model('Bicicleta').find({}, cb);
}

bicicletaSchema.statics.add = function (aBici, cb) {
  this.create(aBici, cb);
}

bicicletaSchema.statics.findByCode = function (aCode, cb) {
  return this.findOne({ code: aCode }, cb);
}

bicicletaSchema.statics.removeByCode = function (aCode, cb) {
  return this.deleteOne({ code: aCode }, cb);
}

module.exports = mongoose.model('Bicicleta', bicicletaSchema);


/*
var Bicicleta = function(id, color, modelo, ciudad, ubicacion) {
  this.id = id;
  this.color = color;
  this.modelo = modelo;
  this.ciudad = ciudad;
  this.ubicacion = ubicacion;
}

Bicicleta.prototype.toString = function() {
  return 'id: ' + this.id + " | color: " + this.color;
}

/* list to save all objetc and his method for add to list */
/* Bicicleta.allBicis = [];
Bicicleta.add = function(aBici) {
  Bicicleta.allBicis.push(aBici);
} */

/* funtion for find in the list for id */
/* Bicicleta.findById = function(aBiciId){
  for (var i = 0; i < Bicicleta.allBicis.length; i++){
    if (Bicicleta.allBicis[i].id == aBiciId){
       return Bicicleta.allBicis[i];
    }
  }
  return "valor no encontrado"
}
 */

/* Create the remove funtion */
/* Bicicleta.removeById = function(aBiciId){
  for (var i = 0; i < Bicicleta.allBicis.length; i++){
    if (Bicicleta.allBicis[i].id == aBiciId){
      Bicicleta.allBicis.splice(i, 1);
      break;
    }
  }
} */


/* Crete two bicis in barranquilla and add to list */
/* var a = new Bicicleta(1, 'blue', 'clasic', "barranquilla", [11.016565, -74.790201]);
var b = new Bicicleta(2, 'blue', 'clasic', "barranquilla", [11.026085, -74.799943]);
Bicicleta.add(a);
Bicicleta.add(b);
*/

/* Crete two bicis in santamarta and add to list */
/*
var c = new Bicicleta(3, 'blue', 'clasic', "santamarta", [11.242083, -74.215416]);
var d = new Bicicleta(4, 'blue', 'clasic', "santamarta", [11.248606, -74.215206]);
Bicicleta.add(c);
Bicicleta.add(d);
*/

/* Crete two bicis in cartagena and add to list */
/*
var e = new Bicicleta(5, 'blue', 'clasic', "cartagena", [10.430245, -75.547298]);
var f = new Bicicleta(6, 'blue', 'clasic', "cartagena", [10.417171, -75.551866]);
Bicicleta.add(e);
Bicicleta.add(f);
*/


/* export models for can use anywhare */
/* module.exports = Bicicleta; */
