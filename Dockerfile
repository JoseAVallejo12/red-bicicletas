# Container for make task coursera course:
# ---Desarrollo del lado servidor: NodeJS, Express y MongoDB--
FROM ubuntu:18.04

# Install packages and utilities
RUN apt-get -y update && apt-get -y install \
	vim \
	curl \
	sudo \
	wget

# Install node version 11.
# For Other vesion remplace setup_10.x or 11.x or 12.x
RUN curl --silent --location https://deb.nodesource.com/setup_11.x  | sudo bash -
RUN sudo apt-get -y update \
	&& sudo apt-get -y install nodejs

RUN export NODE_PATH=/usr/lib/node_modules
RUN rm -rf /var/lib/apt/lists/*

# Install MongoDB
RUN wget -qO - https://www.mongodb.org/static/pgp/server-4.2.asc | apt-key add -
RUN echo "deb [ arch=amd64,arm64 ] https://repo.mongodb.org/apt/ubuntu bionic/mongodb-org/4.2 multiverse" > /etc/apt/sources.list.d/mongodb-org-4.2.list
RUN apt-get update
RUN mkdir -p /data/db
ARG DEBIAN_FRONTEND=noninteractive
ENV TZ=America/Los_Angeles
RUN apt-get install -y tzdata
RUN apt-get install -y mongodb-org


# Install packages...
RUN sudo npm install -g express
RUN sudo npm install -g nodemon

# start run!
